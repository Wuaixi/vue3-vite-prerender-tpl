import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'node:path'
import seoPrerender from 'vite-plugin-seo-prerender'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // https://blog.csdn.net/weixin_65042674/article/details/133902017
    // https://www.npmjs.com/package/vite-plugin-seo-prerender
    seoPrerender({
      routes: ['/', '/prerender'], // 需要生成的路由
      hashHistory: true
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname,'./src')
    }
  }
})
